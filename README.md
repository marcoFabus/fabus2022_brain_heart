# Fabus2022_Brain_Heart

Analysis scripts accompanying Fabus, Sleigh, and Warnaby (2022).

Analysis_ files reproduce data group_ datafiles in Data/, allowing Figures_ files to reproduce all figures in the manuscript.
