#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 11:46:42 2022

@author: MSFabus
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import io, stats, signal
from scipy.ndimage.filters import uniform_filter1d
import yasa
from scipy.interpolate import interp1d
import statsmodels.api as sm

# Filtering helper function
from scipy.signal import butter, sosfiltfilt, welch
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], btype='band', output='sos')
    return sos

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass(lowcut, highcut, fs, order=order)
    y = sosfiltfilt(sos, data)
    return y

plt.rcParams['figure.dpi'] = 200
save = False

#%% Import ECG
fpath = './Data/AlphaMax_out.mat'
raw = io.loadmat(fpath)
eegs = raw['eegs'][0]
hrs = np.squeeze(raw['hrs'])
swas = np.squeeze(raw['swas'])
Ns = len(hrs)

#%% SWA vs HR analysis
sample_rate = int(100)
group_negpeaks = np.zeros((Ns, 4000))*np.nan
group_sw_freqs = np.zeros((Ns, 4000))*np.nan
group_swa_hr = np.zeros((Ns, 2, 6960))

for i in range(Ns):
    print(i)
    data = np.squeeze(eegs[i])
    data_lf = butter_bandpass_filter(data, 0.5, 4, sample_rate)
    amp_thr = np.percentile(np.abs(data_lf[np.abs(data_lf)<200]), 99)

    isw = yasa.sw_detect(data, sample_rate, hypno=None, freq_sw=(0.5, 4),
               dur_neg=(0.25, 1.25), dur_pos=(0, np.inf), amp_neg=(None, None),
               amp_pos=(None, None), amp_ptp=(amp_thr, 200), coupling=False,
               remove_outliers=False, verbose=False)
    isw_sum = isw.summary()
    negpeaks = isw_sum.Start
    Nsw = len(negpeaks)
    group_negpeaks[i, :Nsw] = negpeaks
    group_sw_freqs[i, :Nsw] = isw_sum.Frequency
    
#%% APPENDIX 1 
# Compare frequency of HR and SW
swfs = np.nanmean(group_sw_freqs, 1)
Y = swfs
X = hrs
X = sm.add_constant(X)
model = sm.OLS(Y, X)
res = model.fit()
ypred = res.predict(X)

plt.rc('font', size=16)
fig, ax = plt.subplots()
ax.scatter(hrs, swfs, c='r')
#ax.plot(hrs, ypred, c='k')
ax.set_xticks([40, 50, 60, 70, 80, 90, 100])
ax.set_ylabel('SW Frequency [Hz]')
ax.set_xlabel('Heart Rate [bpm]')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)

if save:
    fname = './Figures/supp_fig_alphamax_hr_swf.png'
    fig.savefig(fname, format='png', bbox_inches = 'tight', dpi=600)