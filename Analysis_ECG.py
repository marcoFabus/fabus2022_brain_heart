#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 14:17:45 2022

Script to reproduce propofol ECG vs EEG analysis 
in Fabus, Sleigh, and Warnaby (2022).

@author: MSFabus
"""
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy import io, stats, signal
from scipy.ndimage.filters import uniform_filter1d
import pyhrv
import biosppy as bs

plt.rcParams['figure.dpi'] = 200

def cohen_d(x,y):
    nx = len(x)
    ny = len(y)
    dof = nx + ny - 2
    return (np.mean(x) - np.mean(y)) / np.sqrt(
        ((nx-1)*np.std(x, ddof=1) ** 2 + (ny-1)*np.std(y, ddof=1) ** 2) / dof)

# Import ECG
data_ecg = io.loadmat('./Data/group_ecg_avg_preproc.mat')['ecg_all']
fs = 500

#%% Go through each subject:
# 1. Extract Q, R, S timings.
# 2. Extract R amplitude, HR, HF RMS, RWA, moving window HRV.
# 3. Mean & std across all subjects
# 4. Figures.

Ns = data.shape[0]
group_ecg = np.zeros((6, Ns, 10000))*np.nan
group_wf = np.zeros((Ns, 50, 300))*np.nan
group_hrv = np.zeros((3, Ns, 50))*np.nan
group_lzw = np.zeros((Ns, 150))*np.nan
group_fft = np.zeros((Ns, 50, 3, 5))*np.nan

for i in range(Ns):
    print('Sub: ', i)
    
    # Extract ECG info
    res = bs.signals.ecg.ecg(data[i, :], sampling_rate=fs, show=False)
    filt_ecg = res[1]
    hr = res[6]; hr_time = res[5]
    rpeaks = res[2]
    Nhb = len(rpeaks)
    hr_templ = res[4]
    
    group_ecg[0, i, :len(hr)] = hr_time
    group_ecg[1, i, :len(hr)] = hr

    # QRS identification
    q_pos, q_start = bs.signals.ecg.getQPositions(res, show=False)
    s_pos, s_end = bs.signals.ecg.getSPositions(res, show=False)
    
    # Extract R amplitude for each beat
    R_amp = np.array([data[i, r] for r in rpeaks])
    
    # Save params: HR time, HR, R peak times, R_amp, mf_rms, hf_rms
    group_ecg[0, i, :len(hr)] = hr_time
    group_ecg[1, i, :len(hr)] = hr
    group_ecg[2, i, :Nhb] = rpeaks
    group_ecg[3, i, :Nhb] = R_amp
    group_ecg[4, i, :Nhb] = mf_rms
    group_ecg[5, i, :Nhb] = hf_rms
    
    print('QRS done.', end=' ')
    
    #5min moving window HRV metrics & shape
    len_win = 302 #s
    Nseg = 23#int(Nhb/len_win)
    hrvres = np.zeros((2, Nseg))

    for k in range(Nseg):
        idx = (rpeaks>k*len_win*fs) & (rpeaks<(k+1)*len_win*fs)
        if np.sum(idx) == 0:
            print('Seg %.0f failed' %k)
            continue
        res_fft = pyhrv.frequency_domain.welch_psd(rpeaks=rpeaks[idx]/fs, show=False, mode='dev') 
        res_nni = pyhrv.time_domain.rmssd(rpeaks=rpeaks[idx]/fs)
        res_hr =  pyhrv.time_domain.hr_parameters(rpeaks=rpeaks[idx]/fs)
        lf_hf = res_fft[0]['fft_ratio']
        
        peak_freq = res_fft[0]['fft_peak']
        fft_abs = res_fft[0]['fft_abs']
        fft_log = res_fft[0]['fft_log']
        fft_total = res_fft[0]['fft_total']
        fft_rel = res_fft[0]['fft_rel']
        
        group_fft[i, k, :, 0] = peak_freq
        group_fft[i, k, :, 1] = fft_abs
        group_fft[i, k, :, 2] = fft_rel
        group_fft[i, k, :, 3] = fft_log
        group_fft[i, k, :, 4] = fft_total
        
        group_hrv[0, i, k] =  res_nni['rmssd']
        group_hrv[1, i, k] =  lf_hf
        group_hrv[2, i, k] =  res_hr['hr_mean']
        
        shape = np.mean(hr_templ[idx, :], 0)
        group_wf[i, k, :] = shape
        
    print('HRV done.', end=' ')
    
np.savez('./Data/group_propofol_ecg.npz', ecg=group_ecg, wf=group_wf, hrv=group_hrv, fft=group_fft)