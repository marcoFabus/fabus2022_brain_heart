%% Code to reproduce clinical dataset analysis in Fabus, Sleigh, and Warnaby (20222).
% MS Fabus, 2022

tbl_group = [];
n = 0;
proprates = [];
ids = [];
hrs = [];
swas = [];
eegs = {};
ctr = 1;

for i = 1:100
     disp(i)
     pt = alphamax(i);
     
     % Only take ASA 2/3
     if pt.asa == 1
         continue
     elseif pt.asa == 4
         continue
     end
     
     % Load data
     ids = [ids; pt.fullID];
     prop0 = pt.prop.fxconc;
     fent0 = pt.fent.fxconc;
     des0 = pt.des.fxconc;
     hr0 = pt.vitalsigns.HR;
   
     % Sometimes propofol starts at t<0. If this is so, we need to zero-pad
     % the rest to have the same length
     [prop, fent] = synchronize(prop0, fent0, 'Union');
     [prop, des] = synchronize(prop0, des0, 'Union');
     [prop, hr] = synchronize(prop0, hr0, 'Union');
     [des, fent] = synchronize(des, fent, 'Union');
     [prop, des] = synchronize(prop, des, 'Union');
     [prop, hr] = synchronize(prop, hr, 'Union');
     
     prop2 = downsample(movmedian(prop.Data, 5), 5);
     fent2 = downsample(movmedian(fent.Data, 5), 5);
     des2 = downsample(movmedian(des.Data, 5), 5);
     hr2 = downsample(movmedian(hr.Data, 24), 5); %2min moving median
     hr2(hr2>250) = NaN;
     hr2(hr2<10) = NaN;
     
     prop_rate = max(diff(prop2)) / 5;
     proprates = [proprates; prop_rate];
     prop_rate = ones(length(prop2), 1)*prop_rate;
     
     ptn = ones(length(prop2), 1)*i;
     age = ones(length(prop2), 1)*pt.age;
     bmi = ones(length(prop2), 1)*pt.bmi;
     asa = ones(length(prop2), 1)*pt.asa;
     gender = repmat(pt.gender, length(prop2), 1);
     fast_loc = ones(length(prop2), 1)*(pt.unresponsive < 4.75);
     bmi_status = ones(length(prop2), 1)*(pt.bmi < 30);

     tbl = table(ptn, age, bmi, bmi_status, asa, gender, fast_loc, prop_rate, prop2, fent2, des2, hr2);
     tbl_group = [tbl_group; tbl];
     
     n = n+1;
end
fail
%% Run GLM
modelspec = ['hr2 ~ 1 + asa + gender + bmi + age + prop2*fent2*des2 - prop2:fent2:des2 +'...
             '(1 | ptn)'];
glme2 = fitglme(tbl_group, modelspec); 
a = table2array(tbl_group(:, 12));
aa  = a(table2array(tbl_group(:, 9))<0.5);
ab  = a(table2array(tbl_group(:, 9))>3);
computeCohen_d(aa, ab)

%% Plot Supplementary Figure
f = figure; hold on
set(gca,'FontSize',18)
scatter(fitted(glme2), table2array(tbl_group(:, end)), 10, 'k', 'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2);
plot(linspace(30, 120, 90), linspace(30, 120, 90), 'r', 'LineWidth', 4)
xlim([30, 120]); ylim([30, 120])
xlabel('Predicted HR [bpm]')
ylabel('Actual HR [bpm]')
exportgraphics(f,'./Figures/supp_fig_GLM.jpg','Resolution',300)