#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 14:17:45 2022

Script to reproduce propofol ECG vs EEG analysis 
in Fabus, Sleigh, and Warnaby (2022).

Specifically: 
    Figure 1, 2

@author: MSFabus
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy import io, stats, signal
from scipy.ndimage.filters import uniform_filter1d
from statsmodels.stats.anova import AnovaRM
import statsmodels.api as sm

plt.rcParams['figure.dpi'] = 200
save = False

def cohen_d(x,y):
    nx = len(x)
    ny = len(y)
    dof = nx + ny - 2
    return (np.mean(x) - np.mean(y)) / np.sqrt(
        ((nx-1)*np.std(x, ddof=1) ** 2 + (ny-1)*np.std(y, ddof=1) ** 2) / dof)

# Import ECG
data_ecg = io.loadmat('./Data/group_ecg_avg_preproc.mat')['ecg_all']
Ns = data_ecg.shape[0]
fs = 500

#%% Load data
data = np.load('./Data/group_propofol_ecg.npz')
ecg = data['ecg']
wf = data['wf']
hrv = data['hrv']
hrv_raw = hrv.copy()
group_fft = data['fft']

conc_path = "./Data/ESCsmooth.mat"
mat = io.loadmat(conc_path)
fxconc = np.array(mat['ESCsmooth'])

#%% FIGURE 1 -----------------------------------------------------------------
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.inset_locator import TransformedBbox, BboxPatch, BboxConnector 
import matplotlib as mpl
import matplotlib.gridspec as gridspec

def mark_inset(parent_axes, inset_axes, loc1a=1, loc1b=1, loc2a=2, loc2b=2, **kwargs):
    rect = TransformedBbox(inset_axes.viewLim, parent_axes.transData)
    pp = BboxPatch(rect, fill=False, **kwargs)
    parent_axes.add_patch(pp)
    p1 = BboxConnector(inset_axes.bbox, rect, loc1=loc1a, loc2=loc1b, **kwargs)
    inset_axes.add_patch(p1)
    p1.set_clip_on(False)
    p2 = BboxConnector(inset_axes.bbox, rect, loc1=loc2a, loc2=loc2b, **kwargs)
    inset_axes.add_patch(p2)
    p2.set_clip_on(False)
    return pp, p1, p2

## Prepare data
Nedge = 24
hr_time = ecg[0, :, :]
hr = ecg[1, :, :]
esc_edges = np.linspace(0, 4, Nedge) 
sec_edges = np.linspace(0, 116*60, Nedge)
params_group = np.zeros((Ns, Nedge-1))
par_std_group = np.zeros((Ns, Nedge-1))
sec_mids = (sec_edges[1:] + sec_edges[:-1]) / 2
esc_mids = np.zeros(Nedge-1)

for i in range(Ns):
    for j in range(Nedge-1):
        sec_min = sec_edges[j]
        sec_max = sec_edges[j+1]
        esc_mids[j] = np.mean(fxconc[int(sec_min):int(sec_max)])
        ix_hr = (hr_time[i, :]>sec_min) & (hr_time[i, :]<sec_max)
        hr_mean = np.nanmean(hr[i, ix_hr])
        hr_std = np.nanstd(hr[i, ix_hr])
        params_group[i, j] = hr_mean
        par_std_group[i, j] = hr_std
        
wfm = np.nanmean(wf, axis=(0))
wfer = np.nanstd(wf, axis=(0)) / np.sqrt(Ns)
wft = np.linspace(-0.2*fs, 0.4*fs, int(0.6*fs))

cm_subsection = [np.linspace(0.1, 1.0, Nedge-1)] 
colors = [ cm.cool(x) for x in esc_mids/4+0.1 ]

x = [['A', 'B'],
     ['A', 'C']]

## Plot data
plt.rc('font', size=14)
fig, axes = plt.subplot_mosaic(x, figsize=(8, 4))
ax3, ax, ax2 = axes.values()
ax.annotate('A', xy=(-200, 1300), fontsize=32, annotation_clip=False)
ax.annotate('B', xy=(350, 1300), fontsize=32, annotation_clip=False)
ax.annotate('C', xy=(350, 300), fontsize=32, annotation_clip=False)

# A: ECG waveform across concentrations
axins = inset_axes(ax, 0.9, 0.9, loc=2, bbox_to_anchor=(0.27, 0.65),
                   bbox_transform=ax.figure.transFigure) # T wave
axins2 = inset_axes(ax, 0.9, 0.7, loc=2, bbox_to_anchor=(0.27, 0.85),
                   bbox_transform=ax.figure.transFigure) # R wave
for i in range(Nedge-2, 0, -1):
    wfm1 = wfm[i, :]
    wfer1 = wfer[i, :]
    ax.plot(wft, wfm1, color=colors[i], lw=1, alpha=0.25)
    axins.plot(wft, wfm1, color=colors[i], lw=1, alpha=0.25)
    axins2.plot(wft, wfm1, color=colors[i], lw=1, alpha=0.25)
    if i == 21:
        ax.plot(wft, wfm1, color=colors[i], lw=0.5, label='Low Propofol')
    if i == 13:
        ax.plot(wft, wfm1, color=colors[i], lw=0.5, label='High Propofol')

ax.set_xlabel('Time [ms]')
ax.set_ylabel('ECG [µV]', fontsize=16)
ax.set_yticks([-500, 0, 500, 1000])
ax.set_yticklabels(['-500', '0', '500', '1000'], fontsize=16)
ax.set_ylim(-500, 1200)
ax.set_xticks([-100, 0, 100, 200])

# sub region of the original image
x1, x2, y1, y2 = 100, 200, -40, 60
axins.set_xlim(x1, x2)
axins.set_ylim(y1, y2)
axins.set_xticks([])
axins.set_yticks([])
mark_inset(ax, axins, loc1a=3, loc1b=2, loc2a=4, loc2b=1, fc="none", ec="0.5", lw=0.5)

x1, x2, y1, y2 = -5, 5, 820, 1100
axins2.set_xlim(x1, x2)
axins2.set_ylim(y1, y2)
axins2.set_xticks([])
axins2.set_yticks([])
mark_inset(ax, axins2, loc1a=3, loc1b=4, loc2a=2, loc2b=1, fc="none", ec="0.5", lw=0.5)

plt.rc('font', size=10)
cax = fig.add_axes([0.158, 0.95, 0.22, 0.05])
cmap = mpl.cm.cool
norm = mpl.colors.Normalize(vmin=0, vmax=4)
cb1 = mpl.colorbar.ColorbarBase(cax, cmap=cmap,
                                norm=norm,
                                orientation='horizontal')
cb1.set_label('Propofol [µg/ml]')
cax.xaxis.set_label_position('top')
cb1.set_ticks([0, 1, 2, 3, 4])
cax.tick_params(axis='x', which='major', pad=1)


# C: group level R peak through concentration
Ramp = ecg[3, :, :]
Ramp = np.nanmax(wf, axis=2)
for i in range(Ns):
    Ramp[i, :] = Ramp[i, :] - np.nanmean(Ramp[i, :])
Rampm = np.nanmean(Ramp, axis=0)[:Nedge-1]
Ramper = np.nanstd(Ramp, axis=0)[:Nedge-1] / np.sqrt(Ns)

col = 'darkmagenta'
plt.rc('font', size=14)
ax3.plot(sec_mids/60, Rampm, color=col)
ax3.fill_between(sec_mids/60, Rampm+Ramper, Rampm-Ramper, alpha=0.5, color=col)
ax3.set_xlabel('Time [min]')
ax3.set_ylabel('R-Wave Amplitude\nchange [µV]', c=col, labelpad=0)
for i in range(Ns):
    ramp = Ramp[i, :Nedge-1] - np.nanmean(Ramp[i, :])
ax3.set_xlabel('Time [min]')
ax3.set_yticks([-100, 0, 100])
ax3.set_xticks([0, 30, 60, 90, 120])
ax3.set_xlim(0, 120)
ax3.tick_params(axis='y', colors=col)
ax32 = ax3.twinx()
ax32.plot(sec_mids/60, esc_mids, c='k', lw=2)
ax32.set_ylabel('Propofol [µg/ml]')
ax32.set_xticks([0, 30, 60, 90, 120])
ax32.set_yticks([0, 2, 4])

# B: Heart rate across concentration
mn = np.mean(params_group, 0)
er = np.std(params_group, 0) / np.sqrt(Ns)
ax2.plot(sec_mids/60, mn, color=col)
ax2.fill_between(sec_mids/60, mn+er, mn-er, alpha=0.5, color=col)
ax2.set_xlabel('Time [min]')
ax2.set_ylabel('Heart Rate\n[bpm]', c=col)
ax2.set_yticks([55, 65, 75])
ax2.set_xticks([0, 30, 60, 90, 120])
ax2.tick_params(axis='y', colors=col)
ax2.set_xlim(0, 120)
ax22 = ax2.twinx()
ax22.plot(sec_mids/60, esc_mids, c='k', lw=2)
ax22.set_ylabel('Propofol [µg/ml]')
ax22.set_yticks([0, 2, 4])

print('RWA vs propofol Spearman:', stats.spearmanr(esc_mids, Rampm))
print('HR vs propofol Spearman:', stats.spearmanr(esc_mids, mn))
print('Cohens d: %.3f' %cohen_d(params_group[:, 12], params_group[:, 0]))

plt.subplots_adjust(wspace=0.7, hspace=0.66)

if save:
    fname = './Figures/fig_1.jpg'
    fig.savefig(fname, format='jpg', bbox_inches = 'tight', dpi=600)
    
plt.show()

#%% STATS FOR FIGURE 1
# RM-ANOVA
df = pd.DataFrame(params_group.T).stack().reset_index().rename(columns={'level_0':'Time', 'level_1':'Sub', 0:'HR'})
aovrm = AnovaRM(df, 'HR', 'Sub', within=['Time'])
res = aovrm.fit()
print('HR vs propofol ANOVA:\n', res.anova_table)

# Linear model to compare with Clinical GLM
Y = mn
X = esc_mids
X = sm.add_constant(X)
model = sm.OLS(Y, X)
res = model.fit()
print('GLM propofol parameters:', res.params, res.bse)



#%% FIGURE 2 -----------------------------------------------------------------
## Prepare data
hrv = hrv.copy()
Nedge = 24
sec_edges = np.linspace(0, 6960, Nedge)
params_group = np.zeros((7, Ns, Nedge-1))
sec_mids = (sec_edges[1:] + sec_edges[:-1]) / 2
esc_mids = np.zeros(Nedge-1)

for i in range(Ns):
    for j in range(Nedge-1):
        sec_min = sec_edges[j]
        sec_max = sec_edges[j+1]
        esc_mids[j] = np.mean(fxconc[int(sec_min):int(sec_max)])
        rmssd_mean = hrv[0, i, j]
        params_group[0, i, j] = rmssd_mean
        lfhf_mean = hrv[1, i, j]
        params_group[1, i, j] = lfhf_mean
        freq_band = 2 # ULF, LF, HF
        # Peak F, abs, rel, log, total FFT
        params_group[2, i, j] = group_fft[i, j, freq_band, 0]
        params_group[3, i, j] = group_fft[i, j, freq_band, 1]
        params_group[4, i, j] = group_fft[i, j, freq_band, 2]
        params_group[5, i, j] = group_fft[i, j, freq_band, 3]
        params_group[6, i, j] = group_fft[i, j, freq_band, 4] 
        
# Labels
ylabs = ['RMSSD [ms]', 'LF/HF', 'Peak f [Hz]', 'Abs', 'Rel Power [%]', 'Log', 'Total']

## Plot data
def get_axis_limits(ax, scale=.9):
    return ax.get_xlim()[1]*(-0.28), ax.get_ylim()[1]*1.02

ylims = [[0, 120], [0, 5], [0.18, 0.32]]
cols = ['royalblue', 'teal', 'steelblue', 'red', 'green', 'blue', 'pink']
labs = ['A', 'B', 'C']
fig, axs = plt.subplots(1, 3, figsize=(12, 2))
for i, p in enumerate([0, 1, 2]):
    ax = axs.flatten()[i]
    mn = np.nanmedian(params_group[p, :, :], 0)
    er = (np.nanpercentile(params_group[p, :, :], 75) - np.nanpercentile(params_group[p, :, :], 25)) / np.sqrt(Ns)

    ax.plot(sec_mids/60, mn, color=cols[p])
    ax.fill_between(sec_mids/60, mn+er, mn-er, alpha=0.5, color=cols[p])
    
    ax.set_ylabel(ylabs[p], c=cols[p])
    ax.set_ylim(ylims[i])
    ax2 = ax.twinx()
    ax2.plot(sec_mids/60, esc_mids, c='k', lw=2)
    ax2.set_ylabel('Propofol [µg/ml]')
    ax.tick_params(axis='y', colors=cols[p])
    ax.set_xlabel('Time [min]')
    ax.set_xticks([0, 30, 60, 90, 120])
    ax.annotate(labs[i], xy=get_axis_limits(ax), fontsize=28, annotation_clip=False)
    
    print(ylabs[i], 'Spearman:', stats.spearmanr(esc_mids, mn))

plt.subplots_adjust(wspace=0.75)

if save:
    fname = './Figures/fig_2.jpg'
    fig.savefig(fname, format='jpg', bbox_inches = 'tight', dpi=600)
plt.show()

#%% STATS FOR FIGURE 2 
# RM-ANOVA
from statsmodels.stats.anova import AnovaRM
df = pd.DataFrame(params_group[0, :, :].T).stack().reset_index().rename(columns={'level_0':'Time', 'level_1':'Sub', 0:'RMSSD'})
aovrm = AnovaRM(df, 'RMSSD', 'Sub', within=['Time'])
res = aovrm.fit()
print('RMSSD ANOVA:\n', res.anova_table)

print('Cohens d: %.3f' %cohen_d(params_group[1, :, 12], params_group[1, :, 0]))

Y = np.nanmedian(params_group[0, :, :], 0)
X = esc_mids
X = sm.add_constant(X)
model = sm.OLS(Y, X)
res = model.fit()
print('RMSSD GLM:', res.params, res.bse)