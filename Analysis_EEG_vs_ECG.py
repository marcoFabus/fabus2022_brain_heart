#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 14:17:45 2022

Script to reproduce propofol ECG vs EEG analysis 
in Fabus, Sleigh, and Warnaby (2022).

@author: MSFabus
"""
import numpy as np
from scipy import io, stats, signal
import biosppy as bs
import yasa
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter, stft

# Filtering helper function
from scipy.signal import butter, sosfiltfilt, welch
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], btype='band', output='sos')
    return sos

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass(lowcut, highcut, fs, order=order)
    y = sosfiltfilt(sos, data)
    return y

def find_swa(data, sample_rate, freq=[0.49, 1.51], method='dB', smooth=True):
    f, t, Z = stft(data, sample_rate, nperseg=4*sample_rate, noverlap=3*sample_rate, nfft=4*sample_rate)
    SWA_f = np.logical_and(f>freq[0], f<freq[1])
    ps = np.abs(Z)
    SWA_uV = np.mean(ps[SWA_f, :], 0)
    if method == 'dB':
        SWA = 10*np.log(SWA_uV)
        if smooth:
            SWA_smooth = savgol_filter(SWA, 51, 3)
        else:
            SWA_smooth = None
    return [SWA, SWA_smooth]

#%% Import ECG
data_ecg = io.loadmat('./Data/group_ecg_avg_preproc.mat')['ecg_all']
Ns = data_ecg.shape[0]
fs = 500

#%% SWA vs HR analysis
eeg_dir = "/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/Concatenated_final/preproc/"
subjects = ['001', '003', '004', '005', '007', '008', '011', '013', '014', '016', '017', '019', '020', '021', '022', '023']
sample_rate = int(100)

data_ecg = io.loadmat(datadir+fpath_ecg)['ecg_all']
group_ecg_wf = np.zeros((Ns, int(4*fs-1)))
group_lf_ecg_wf = np.zeros((Ns, int(4*fs-1)))
group_eeg_wf = np.zeros((Ns, int(4*sample_rate-1)))
group_RS_timings = np.zeros((Ns, 3000, 8))*np.nan
group_RS_timings_norm = np.zeros((Ns, 3000, 8))*np.nan

group_ihr = np.zeros((Ns, 3000))*np.nan
group_negpeaks = np.zeros((Ns, 3000))*np.nan
group_sw_freqs = np.zeros((Ns, 3000))*np.nan
group_swa_hr = np.zeros((Ns, 2, 6960))

for i in range(Ns):
    print(i)
    
    fname = eeg_dir + 'sub_' + subjects[i] + '_eeg_avg_preproc.mat'
    data = io.loadmat(fname)['EEG_preproc']
    data = data[16, :] #16 = Fz
    ecg = data_ecg[i, :]
    
    data_lf = butter_bandpass_filter(data, 0.5, 4, sample_rate)
    amp_thr = np.percentile(np.abs(data_lf[np.abs(data_lf)<200]), 99)

    isw = yasa.sw_detect(data, sample_rate, hypno=None, freq_sw=(0.5, 4),
               dur_neg=(0.25, 1.25), dur_pos=(0, np.inf), amp_neg=(None, None),
               amp_pos=(None, None), amp_ptp=(amp_thr, 200), coupling=False,
               remove_outliers=False, verbose=False)
    isw_sum = isw.summary()
    negpeaks = isw_sum.Start
    Nsw = len(negpeaks)
    group_negpeaks[i, :Nsw] = negpeaks
    group_sw_freqs[i, :Nsw] = isw_sum.Frequency
    
    # Extract ECG parameters    
    res = bs.signals.ecg.ecg(ecg, sampling_rate=fs, show=False)
    
    # Prepare individual SWA and HR
    hr = res[6]; hr_time = res[5]
    [swa, swa_smooth] = find_swa(data, sample_rate)
    swa_short = swa[:6960]
    
    # Interpolate HR every second
    hr_interp_fn = interp1d(hr_time, hr, kind='cubic', fill_value='extrapolate')
    hr_1Hz = hr_interp_fn(np.linspace(0, len(swa_short), len(swa_short)))
    group_swa_hr[i, 0, :] = swa_short
    group_swa_hr[i, 1, :] = hr_1Hz
    
    # For each slow wave onset, find closest r peaks: RS+/- 1, 2, 3, 4
    # As per Galletly papers
    rpeaks = res[2] / fs
    
    for j in range(5, Nsw-5):
        t_sw = negpeaks[j]
        RS_pos = np.sort(rpeaks - t_sw)[np.sort(rpeaks - t_sw) > 0][:4]
        RS_neg = np.sort(rpeaks - t_sw)[np.sort(rpeaks - t_sw) < 0][-4:]
        group_RS_timings[i, j, :] = np.hstack([RS_neg, RS_pos])
        ihr = 8/(group_RS_timings[i, j, -1] - group_RS_timings[i, j, 0]) #HR, beats per second (Hz)
        group_ihr[i, j] = ihr
        group_RS_timings_norm[i, j, :] = np.hstack([RS_neg, RS_pos]) * ihr


    lf_ecg, _, _ = bs.signals.tools.filter_signal(ecg, ftype='butter', 
                                            band='bandpass', order=3, 
                                            frequency=[0.5, 4], sampling_rate=fs)
    
    # For each slow wave, find evoked ECG around neg peak
    ecg_wf_sub = np.zeros((10000, int(4*fs-1)))*np.nan
    eeg_wf_sub = np.zeros((10000, int(4*sample_rate-1)))*np.nan
    lf_ecg_wf_sub = np.zeros((10000, int(4*fs-1)))*np.nan
    filt_ecg = res[1]
    t_ecg = res[0]
    t_eeg = np.linspace(0, len(data)/sample_rate, len(data))
    for j in range(5, Nsw-5):
        t_sw = negpeaks[j]
        ix_eeg = (t_eeg>t_sw-2) & (t_eeg<t_sw+2)
        eeg_wf = data[ix_eeg]
        eeg_wf_sub[j, :] = eeg_wf[:399]
        
        ix_ecg = (t_ecg>t_sw-2) & (t_ecg<t_sw+2)
        ecg_wf = ecg[ix_ecg]
        ecg_wf_sub[j, :] = ecg_wf[:1999]
        ecg_wf = lf_ecg[ix_ecg]
        lf_ecg_wf_sub[j, :] = ecg_wf[:1999]
        
    eeg_wf_mean = np.nanmean(eeg_wf_sub, 0)
    group_eeg_wf[i, :] = eeg_wf_mean
    
    ecg_wf_mean = np.nanmean(ecg_wf_sub, 0)
    group_ecg_wf[i, :] = ecg_wf_mean
    
    lf_ecg_wf_mean = np.nanmean(lf_ecg_wf_sub, 0)
    group_lf_ecg_wf[i, :] = lf_ecg_wf_mean

#%% Save results
np.savez('group_prop_ecg_v_eeg.npz', wf_eeg=group_eeg_wf, wf_ecg=group_ecg_wf, 
         wf_lf_ecg=group_lf_ecg_wf, Rnorm=group_RS_timings_norm,
         Rs=group_RS_timings, swa_hr=group_swa_hr, ihr=group_ihr, negpeaks=group_negpeaks,
         sw_freq=group_sw_freqs)

