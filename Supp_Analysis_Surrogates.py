#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  1 09:32:32 2022

Surrogate test for cortico-cardiac coupling in Fabus, Sleigh, and Warnaby (2022)

Method:
    - Simulate 10min of EEG data (1Hz bursts + 1/f activity)
    - Simulate same length ECG data
    - Repeat with random slow waves
    
    
Main points:
    - Two oscillations of similar frequency are naturally coupled
    - If SW are a random process, not so
@author: MSFabus
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import io, stats, signal
from scipy.ndimage.filters import uniform_filter1d
import pyhrv
import biosppy as bs
import neurokit2 as nk
import neurodsp.sim as ndp
import yasa
import statsmodels.api as sm
from neurodsp.utils import set_random_seed, create_times

import sys, os
sys.path.insert(0, "/home/marco/Insync/sjoh4485@ox.ac.uk/OneDrive Biz/PhD Year 1/EMD/Analysis Code/")
import slowak as sw

plt.rcParams['figure.dpi'] = 200

def entropy(Pb, N):
    entr = -1 * np.nansum(Pb * np.log(Pb))
    max_entr = -1 * np.log(1/N)
    return entr / max_entr

#%% Simulate signals
seed = 4
fs = 100
N_sec = 10*60
hr0 = 60
lim = 3
Nperms = 5
Nbins = 10
Nfreq = 1
freqs = [1.] # CHANGE: A, B - 1Hz; C, D - 1.1Hz
entropies = np.zeros((Nperms, Nfreq))

# Simulate ECG
ecg = nk.ecg_simulate(duration=N_sec, sampling_rate=fs, noise=0.01, heart_rate=hr0,
                      heart_rate_std=1, random_state=seed)

for ii in range(Nfreq):
    print(ii, end=' ')
    for p in range(Nperms):
        # Simulate EEG
        set_random_seed(p)
        sim_components = {'sim_powerlaw': {'exponent' : -1.5},
                          'sim_bursty_oscillation' : [{'freq' : freqs[ii], 'enter_burst' : .75, 'leave_burst' : .5},
                                                      {'freq' : 10, 'enter_burst' : .5, 'leave_burst' : .5}]}
        eeg = ndp.sim_combined(n_seconds=N_sec, fs=fs, components=sim_components,
                                component_variances=[0.5, 0, 0.25])
        eeg *= 10
        
        # Add random slow waves
        ts = np.linspace(0, 1/freqs[ii], int(fs/freqs[ii]))
        slow_wave = 25*np.sin(2*np.pi*freqs[ii]*ts) 
        Nsw0 = 200
        
        # B - slow waves at random times
        sw_times = np.linspace(5, N_sec-5, Nsw0) + np.random.uniform(-1, 1, Nsw0)
        
        # A, C, D - slow waves at coupled times
        #sw_times = np.arange(5, 595, 3)
        
        for s in range(len(sw_times)):
            eeg[int(sw_times[s]*fs):int(sw_times[s]*fs+len(slow_wave))] = slow_wave + np.random.normal(0, 5, len(ts))

        # Perform analysis
        Ns = 1; i = 0
        group_RS_timings = np.zeros((Ns, 3000, 8))*np.nan
        group_RS_timings_norm = np.zeros((Ns, 3000, 8))*np.nan
        group_ihr = np.zeros((Ns, 3000))*np.nan
        group_negpeaks = np.zeros((Ns, 3000))*np.nan
        data_lf = sw.preproc.butter_bandpass_filter(eeg, 0.5, 4, fs)
        amp_thr = np.percentile(np.abs(data_lf[np.abs(data_lf)<200]), 99)
        isw = yasa.sw_detect(eeg, fs, hypno=None, freq_sw=(0.5, 4),
                   dur_neg=(0.25, 1.25), dur_pos=(0, np.inf), amp_neg=(10, None),
                   amp_pos=(None, None), amp_ptp=(amp_thr, 200), coupling=False,
                   remove_outliers=False, verbose=False)
        isw_sum = isw.summary()
        negpeaks = isw_sum.Start
        Nsw = len(negpeaks)
        group_negpeaks[i, :Nsw] = negpeaks
        
        # Extract ECG parameters
        res = bs.signals.ecg.ecg(ecg, sampling_rate=fs, show=False)
        hr = res[6]; hr_time = res[5]
        filt_ecg = res[1]
        t_ecg = res[0]
        
        # For each slow wave onset, find closest r peaks: RS+/- 1, 2, 3, 4
        rpeaks = res[2] / fs
        
        for j in range(5, Nsw-5):
            t_sw = negpeaks[j]
            RS_pos = np.sort(rpeaks - t_sw)[np.sort(rpeaks - t_sw) > 0][:4]
            RS_neg = np.sort(rpeaks - t_sw)[np.sort(rpeaks - t_sw) < 0][-4:]
            group_RS_timings[i, j, :] = np.hstack([RS_neg, RS_pos])
            ihr = 8/(group_RS_timings[i, j, -1] - group_RS_timings[i, j, 0]) #HR, beats per second / Hz
            group_ihr[i, j] = ihr
            group_RS_timings_norm[i, j, :] = np.hstack([RS_neg, RS_pos]) * ihr
            
        # For each slow wave, find evoked ECG around neg peak
        lf_ecg, _, _ = bs.signals.tools.filter_signal(ecg, ftype='butter', 
                                                band='bandpass', order=3, 
                                                frequency=[0.5, 4], sampling_rate=fs)
        ecg_wf_sub = np.zeros((10000, int(4*fs-1)))*np.nan
        eeg_wf_sub = np.zeros((10000, int(4*fs-1)))*np.nan
        lf_ecg_wf_sub = np.zeros((10000, int(4*fs-1)))*np.nan
        filt_ecg = res[1]
        t_ecg = res[0]
        t_eeg = np.linspace(0, len(eeg)/fs, len(eeg))
        for j in range(5, Nsw-5):
            t_sw = negpeaks[j]
            ix_eeg = (t_eeg>t_sw-2) & (t_eeg<t_sw+2)
            eeg_wf = eeg[ix_eeg]
            eeg_wf_sub[j, :] = eeg_wf[:399]
            
            ix_ecg = (t_ecg>t_sw-2) & (t_ecg<t_sw+2)
            ecg_wf = ecg[ix_ecg]
            ecg_wf_sub[j, :] = ecg_wf[:399]
            ecg_wf = lf_ecg[ix_ecg]
            lf_ecg_wf_sub[j, :] = ecg_wf[:399]    
        
        # ACF method
        rs = group_RS_timings[0, :, :].flatten()
        hist_uni, be = np.histogram(rs, range=(-lim, lim), bins=Nbins)
        acf_uni = sm.tsa.stattools.acf(hist_uni, nlags=Nbins)[1:]
        
        # Galletly entropy method
        rs = group_RS_timings[0, :, 3]
        rs = np.abs(rs[~np.isnan(rs)])
        hist_uni, be = np.histogram(rs, range=(0, 60/hr0), bins=Nbins)
        hist_uni = hist_uni / len(rs)
        entr = entropy(hist_uni, Nbins)
        entropies[p, ii] = entr

print('Entropy:', entr)


#%% SUPPLEMENTARY FIGURE 7 ---------------------------------------------------
i = 0
# Raw EEG, ECG
plt.rc('font', size=24)
fig = plt.figure(figsize=(16, 8)) 
ax0 = fig.add_axes([0, 0.5, 0.6, 0.25])
ax1 = fig.add_axes([0.68, 0.5, 0.275, 0.25])
ax2 = fig.add_axes([0, -0.2, 0.6, 0.6])
ax3 = fig.add_axes([0.66, -0.1, 0.3, 0.4])

# A: Raw EEG, ECG
mask = isw.get_mask()
times = np.arange(eeg.size) / fs
sw_highlight = eeg * mask
sw_highlight[sw_highlight == 0] = np.nan
events = isw_sum
ax0.plot(times, eeg, 'k')
pt = 'Start' 
ax0.plot(t_ecg, filt_ecg*100-50, 'red', alpha=0.95)
ax0.scatter(events[pt], sw_highlight[(events[pt] * fs).astype(int)], 
         c='steelblue', label='Negative peaks', s=100)
[ax0.axvline(x, c='r', alpha=0.5, ymin=0.05, ymax=1) for x in rpeaks[:300]]
ax0.set_xlim(45, 60)
ax0.axis('off')

# B: Recurrence plot
for k in range(1, 6):
    RS = group_RS_timings[0, :, k]
    negpeaks = group_negpeaks[i, :]
    ax1.scatter(negpeaks/60, RS, s=1, c='k')
ax1.set_xlabel('Time [min]')
ax1.set_ylabel('R-S interval [s]')
ax1.set_ylim(-2.5, 2.)
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)

# C: Averaged ECG & SW
tt = np.linspace(-2, 2, 399)
tt2 = np.linspace(-2, 2, 399)
mn1 = np.nanmean(eeg_wf_sub[:, :]/1, 0)
er1 = np.nanstd(eeg_wf_sub/1, 0) / np.sqrt(Nsw)
mn2 = np.nanmean(ecg_wf_sub[:, :], 0)
er2 = np.nanstd(ecg_wf_sub, 0) / np.sqrt(len(rpeaks))
mn3 = np.nanmean(lf_ecg_wf_sub[:, :], 0)
er3 = np.nanstd(lf_ecg_wf_sub, 0) / np.sqrt(len(rpeaks))
mn2 -= np.mean(mn2)
ax22 = ax2.twinx()
l1, = ax2.plot(tt, mn1, c='b', label='EEG')
ax2.fill_between(tt, mn1+er1, mn1-er1, alpha=0.5, color='b')
l2, = ax22.plot(tt2, mn2, c='r', label='ECG', lw=1, alpha=0.75)
ax22.fill_between(tt2, mn2+er2, mn2-er2, alpha=0.5, color='r')
l3, = ax22.plot(tt2, mn3, c='purple', label='LF-ECG', lw=2)
ax22.fill_between(tt2, mn3+er3, mn3-er3, alpha=0.5, color='purple')
ax2.set_xlabel('Time from slow wave start [s]')
ax2.set_ylabel('10μV', labelpad=-10, fontsize=30)
ax2.arrow(-2.1, -7, 0, 10, lw=6, head_width=0, color='k')
ax2.set_xticks([-2, -1, 0, 1, 2])
ax2.set_yticks([])
ax22.set_yticks([])
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax2.spines['left'].set_visible(False)
ax22.spines['left'].set_visible(False)
ax22.spines['right'].set_visible(False)
ax22.spines['top'].set_visible(False)
ax2.grid()
leg = ax2.legend((l1, l2, l3), ('Slow Wave', 'ECG', 'LF-ECG'), loc = 'lower center', 
              ncol=3, labelspacing=0. , bbox_to_anchor=(0.51, 0.9))
for line in leg.get_lines():
    line.set_linewidth(10.0)

# D: Histogram
lim = 3
Nbins = 90
ihr = np.nanmean(group_ihr, 1)[i]
rs = group_RS_timings[i, :, :].flatten() #* ihr
rs = rs[~np.isnan(rs)]
rs = rs[np.abs(rs)<lim]   
ax3.hist(rs, bins=Nbins, range=(-lim, lim), color='skyblue', ec='k', lw=0.25, density=True)
ax3.set_xticks([-3 ,-2, -1, 0, 1, 2, 3])
ax3.set_ylabel('R-wave incidence')
ax3.set_xlabel('Time from slow wave start [s]')