#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 14:17:45 2022

Script to reproduce propofol ECG vs EEG figures
in Fabus, Sleigh, and Warnaby (2022).

Specifically: 
    Figure 3, 4, 5
    Supplementary Figure 2, 4, 5, 6

@author: MSFabus
"""
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import io, stats, signal
import biosppy as bs
import yasa
import statsmodels.api as sm
from scipy.optimize import curve_fit
from sklearn.metrics import r2_score

# Filtering helper function
from scipy.signal import butter, sosfiltfilt, welch
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], btype='band', output='sos')
    return sos

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass(lowcut, highcut, fs, order=order)
    y = sosfiltfilt(sos, data)
    return y

plt.rcParams['figure.dpi'] = 200

save = False

#%% Import ECG and propofol concentration
data_ecg = io.loadmat('./Data/group_ecg_avg_preproc.mat')['ecg_all']
Ns = data_ecg.shape[0]
fs = 500

conc_path = "./Data/ESCsmooth.mat"
mat = io.loadmat(conc_path)
fxconc = np.array(mat['ESCsmooth'])

#%% SWA vs HR analysis
eeg_dir = "/media/marco/MSFabus_DPhil2/Datasets/propofol_bench/Concatenated_final/preproc/"
subjects = ['001', '003', '004', '005', '007', '008', '011', '013', '014', '016', '017', '019', '020', '021', '022', '023']
sample_rate = int(100)

#%% Load results
# Change to _negpeak for Supplementary Figures, _mastoid for mastoid re-referenced
dat = np.load('./Data/group_prop_ecg_v_eeg.npz') 
group_swa_hr = dat['swa_hr']
group_eeg_wf = dat['wf_eeg']
group_ecg_wf = dat['wf_ecg']
group_lf_ecg_wf = dat['wf_lf_ecg']
group_ihr = dat['ihr']
group_negpeaks = dat['negpeaks']
group_RS_timings = dat['Rs']
group_sw_freqs = dat['sw_freq']
fail
#%% FIGURE 3 -----------------------------------------------------------------
## Prepare data
swam = np.nanmean(group_swa_hr[:, 0, :], 0)
swaer = np.nanstd(group_swa_hr[:, 0, :], 0) / np.sqrt(Ns)
hrm = np.nanmean(group_swa_hr[:, 1, :], 0)
hrer = np.nanstd(group_swa_hr[:, 1, :], 0)  / np.sqrt(Ns)

t = np.linspace(0, 116, 6960)

Nedge = 24
sec_edges = np.linspace(0, 6960, Nedge)
sec_mids = (sec_edges[1:] + sec_edges[:-1]) / 2
esc_mids = np.zeros(Nedge-1)
swa_mids = np.zeros(Nedge-1)
swa_mids_subs = np.zeros((Ns, Nedge-1))
hr_mids_subs = np.zeros((Ns, Nedge-1))
swaer_mids = np.zeros(Nedge-1)
hr_mids = np.zeros(Nedge-1)
hrer_mids = np.zeros(Nedge-1)

for j in range(Nedge-1):
    sec_min = sec_edges[j]
    sec_max = sec_edges[j+1]
    esc_mids[j] = np.mean(fxconc[int(sec_min):int(sec_max)])
    swa_mids[j] = np.mean(swam[int(sec_min):int(sec_max)])
    swaer_mids[j] = np.mean(swaer[int(sec_min):int(sec_max)])
    hr_mids[j] = np.mean(hrm[int(sec_min):int(sec_max)])
    hrer_mids[j] = np.mean(hrer[int(sec_min):int(sec_max)])
    for i in range(Ns):
        swa_mids_subs[i, j] = np.nanmean(group_swa_hr[i, 0, int(sec_min):int(sec_max)])
        hr_mids_subs[i, j] = np.nanmean(group_swa_hr[i, 1, int(sec_min):int(sec_max)])
            
## Plot data
col = 'darkorchid'
plt.rc('font', size=20)
fig, (ax, ax3) = plt.subplots(1, 2, figsize=(12, 3)) 
ax.annotate('A', xy=(-45, 15), fontsize=40, annotation_clip=False)
ax.annotate('B', xy=(170, 15), fontsize=40, annotation_clip=False)

# A: SWA-ESC
ax.plot(sec_mids/60, swa_mids, color=col)
ax.fill_between(sec_mids/60, swa_mids+swaer_mids, swa_mids-swaer_mids, alpha=0.5, color=col)
ax.set_yticks([ -5, 0, 5, 10, 15])
ax.set_ylabel('Slow-Wave\nActivity [dB]', color=col)
ax.set_xlabel('Time [min]')
ax.set_xticks([0, 30, 60, 90, 120])
ax.tick_params(axis='y', colors=col)

ax2 = ax.twinx()
ax2.plot(t, fxconc, c='k', lw=2)
ax2.set_ylabel('Propofol [µg/ml]')

# B: SWA-HR
ax3.scatter(hr_mids, swa_mids, s=25, c=col, zorder=0)
ax3.errorbar(hr_mids, swa_mids, yerr=swaer_mids, xerr=hrer_mids, ls='none', color=col, capsize=3,
            alpha=0.25, zorder=2)
ax3.set_yticks([ -5, 0, 5, 10, 15])
ax3.set_xticks([55, 60, 65, 70, 75])
ax3.set_ylabel('Slow-Wave\nActivity [dB]')
ax3.set_xlabel('Heart Rate [bpm]')
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)

plt.subplots_adjust(wspace=0.66)
print('SWA-ESC test: ', stats.spearmanr(esc_mids, swa_mids))
print('SWA-HR test: ', stats.spearmanr(hr_mids, swa_mids))

if save:
    fname = './Figures/fig_3.jpg'
    fig.savefig(fname, format='jpg', bbox_inches = 'tight', dpi=600)
    
plt.show()


#%% FIGURE 4 -----------------------------------------------------------------
## Prepare data
i = 5
fname = eeg_dir + 'sub_' + subjects[i] + '_eeg_avg_preproc.mat'
data = io.loadmat(fname)['EEG_preproc'][16, :]
data_lf = butter_bandpass_filter(data, 0.5, 4, sample_rate)
ecg = data_ecg[i, :]
amp_thr = np.percentile(np.abs(data_lf[np.abs(data_lf)<200]), 99)

isw = yasa.sw_detect(data, sample_rate, hypno=None, freq_sw=(0.5, 4),
           dur_neg=(0.25, 1.25), dur_pos=(0, np.inf), amp_neg=(None, None),
           amp_pos=(None, None), amp_ptp=(amp_thr, 200), coupling=False,
           remove_outliers=False, verbose=False)
isw_sum = isw.summary()
negpeaks = isw_sum.Start
Nsw = len(negpeaks)
res = bs.signals.ecg.ecg(ecg, sampling_rate=fs, show=False)
rpeaks = res[2] / fs
filt_ecg = res[1]
t_ecg = res[0]
lf_ecg_wf_sub = np.zeros((10000, int(4*fs-1)))*np.nan
ecg_wf_sub = np.zeros((10000, int(4*fs-1)))*np.nan
eeg_wf_sub = np.zeros((10000, int(4*sample_rate-1)))*np.nan
t_eeg = np.linspace(0, len(data)/sample_rate, len(data))

lf_ecg, _, _ = bs.signals.tools.filter_signal(ecg, ftype='butter', 
                                        band='bandpass', order=3, 
                                        frequency=[0.5, 4], sampling_rate=fs)
    
# For each slow wave onset, find closest r peaks: RS+/- 1, 2, 3, 4
for j in range(5, Nsw-5):
    t_sw = negpeaks[j]
    RS_pos = np.sort(rpeaks - t_sw)[np.sort(rpeaks - t_sw) > 0][:4]
    RS_neg = np.sort(rpeaks - t_sw)[np.sort(rpeaks - t_sw) < 0][-4:]

    group_RS_timings[i, j, :] = np.hstack([RS_neg, RS_pos])
    ihr = 8/(group_RS_timings[i, j, -1] - group_RS_timings[i, j, 0]) #HR, beats per second / Hz
    group_ihr[i, j] = ihr
    ix_eeg = (t_eeg>t_sw-2) & (t_eeg<t_sw+2)
    eeg_wf = data[ix_eeg]
    eeg_wf_sub[j, :] = eeg_wf[:399]
    
    ix_ecg = (t_ecg>t_sw-2) & (t_ecg<t_sw+2)
    ecg_wf = ecg[ix_ecg]
    ecg_wf_sub[j, :] = ecg_wf[:1999]
    ecg_wf = lf_ecg[ix_ecg]
    lf_ecg_wf_sub[j, :] = ecg_wf[:1999]

## Plot data
plt.rc('font', size=24)
fig = plt.figure(figsize=(16, 8)) 
ax0 = fig.add_axes([0, 0.5, 0.6, 0.25])
ax1 = fig.add_axes([0.68, 0.5, 0.275, 0.25])
ax2 = fig.add_axes([0, -0.2, 0.6, 0.6])
ax3 = fig.add_axes([0.66, -0.1, 0.3, 0.4])
ax1.annotate('D', xy=(47.5, -5.5), fontsize=60, annotation_clip=False)
ax1.annotate('C', xy=(24, -5.5), fontsize=60, annotation_clip=False)
ax1.annotate('A', xy=(24, 2.7), fontsize=60, annotation_clip=False)
ax1.annotate('B', xy=(47.5, 2.7), fontsize=60, annotation_clip=False)

# A: Raw EEG, ECG
mask = isw.get_mask()
times = np.arange(data.size) / sample_rate
sw_highlight = data * mask
sw_highlight[sw_highlight == 0] = np.nan
events = isw_sum

ax0.plot(times, data, 'k')
pt = 'Start'
ax0.plot(t_ecg, filt_ecg/10-100, 'red', alpha=0.95)
ax0.scatter(events[pt], sw_highlight[(events[pt] * sample_rate).astype(int)], 
         c='steelblue', label='Negative peaks', s=100)
[ax0.axvline(x, c='r', alpha=0.5, ymin=0.05, ymax=1) for x in rpeaks[3650:3900]]
ax0.axvline(3463.82, c='b', alpha=0.75, lw=3, zorder=1)
ax0.arrow(3463.82, -135, -1.15, 0, lw=6, head_width=0, color='b')
ax0.arrow(3463.82, -150, -0.38, 0, lw=6, head_width=0, color='b')
ax0.arrow(3463.82, -165, 0.38, 0, lw=6, head_width=0, color='b')
ax0.arrow(3463.82, -180, 1.15, 0, lw=6, head_width=0, color='b')
ax0.arrow(3455, -185, 2, 0, lw=6, head_width=0, color='k')
ax0.set_xlim(3455, 3470)
ax0.set_ylim(-190, 30)
ax0.annotate('RS intervals', xy=(3464.95, -165), fontsize=28, annotation_clip=False, color='b')
ax0.annotate('ECG', xy=(3454, -90), fontsize=28, annotation_clip=False)
ax0.annotate('EEG', xy=(3454, 10), fontsize=28, annotation_clip=False)
ax0.annotate('2s', xy=(3455, -170), fontsize=24, annotation_clip=False)
ax0.axis('off')

# B: Recurrence plot
for k in range(1, 6):
    RS = group_RS_timings[5, :, k]
    negpeaks = group_negpeaks[i, :]
    ax1.scatter(negpeaks/60, RS, s=1, c='k')
    ax1.set_xlim(3000/60, 3600/60)
ax1.set_xlabel('Time [min]')
ax1.set_ylabel('RS interval [s]')
ax1.set_ylim(-2.5, 2.)
ax1.set_xticks([50, 55, 60])
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)

# C: Averaged ECG & SW
tt = np.linspace(-2, 2, 399)
tt2 = np.linspace(-2, 2, 1999)
mn1 = np.nanmean(eeg_wf_sub[:, :]/1, 0)
er1 = np.nanstd(eeg_wf_sub/1, 0) / np.sqrt(Nsw)
mn2 = np.nanmean(ecg_wf_sub[:, :], 0)
er2 = np.nanstd(ecg_wf_sub, 0) / np.sqrt(len(rpeaks))
mn3 = np.nanmean(lf_ecg_wf_sub[:, :], 0)
er3 = np.nanstd(lf_ecg_wf_sub, 0) / np.sqrt(len(rpeaks))

ax22 = ax2.twinx()
l1, = ax2.plot(tt, mn1, c='b', label='EEG')
ax2.fill_between(tt, mn1+er1, mn1-er1, alpha=0.5, color='b')
l2, = ax22.plot(tt2, mn2, c='r', label='ECG', lw=1, alpha=0.75)
ax22.fill_between(tt2, mn2+er2, mn2-er2, alpha=0.5, color='r')
l3, = ax22.plot(tt2, mn3, c='purple', label='LF-ECG', lw=2)
ax22.fill_between(tt2, mn3+er3, mn3-er3, alpha=0.5, color='purple')
ax2.set_xlabel('Time from slow wave start [s]')
ax2.set_ylabel('10μV', labelpad=-10, fontsize=30)
ax2.arrow(-2.1, -7, 0, 10, lw=6, head_width=0, color='k')
ax2.set_ylim(-13, 10)
ax22.set_ylim(-13, 10)
ax2.set_xticks([-2, -1, 0, 1, 2])
ax2.set_yticks([])
ax22.set_yticks([])
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax2.spines['left'].set_visible(False)
ax22.spines['left'].set_visible(False)
ax22.spines['right'].set_visible(False)
ax22.spines['top'].set_visible(False)
ax2.grid()
leg = ax2.legend((l1, l2, l3), ('Slow Wave', 'ECG', 'LF-ECG'), loc = 'lower center', 
              ncol=3, labelspacing=0. , bbox_to_anchor=(0.51, 0.9))
for line in leg.get_lines():
    line.set_linewidth(10.0)

# D: Histogram
lim = 3
Nbins = 90
ihr = np.nanmean(group_ihr, 1)[i]
rs = group_RS_timings[i, :, :].flatten() #* ihr
rs = rs[~np.isnan(rs)]
rs = rs[np.abs(rs)<lim]   
ax3.hist(rs, bins=Nbins, range=(-lim, lim), color='skyblue', ec='k', lw=0.25, density=True)
ax3.set_xticks([-3 ,-2, -1, 0, 1, 2, 3])
ax3.set_yticks([0, 0.1, 0.2])
ax3.set_ylabel('R-wave incidence')
ax3.set_xlabel('Time from slow wave start [s]')
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)

if save:
    fname = './Figures/fig_4.jpg'
    fig.savefig(fname, format='jpg', bbox_inches = 'tight', dpi=600)
    
plt.show()


#%% FIGURE 5 -----------------------------------------------------------------
## Prepare data
def entropy(Pb, N):
    # GALLETLY STYLE: entropy of RS_{-1}
    entr = -1 * np.nansum(Pb * np.log(Pb))
    max_entr = -1 * np.log(1/N)
    return entr / max_entr

# Random surrogate
Nperm = 10000
Nbins = 10
Npts = 200
perms = np.zeros(Nperm)
for p in range(Nperm):
    np.random.seed(p*2)
    uni = np.random.uniform(0, 1, Npts)
    hist_uni, be = np.histogram(uni, range=(0, 1), bins=Nbins)
    hist_uni = hist_uni / Npts
    entr = entropy(hist_uni, Nbins)
    perms[p] = entr
th = np.percentile(perms, 1)

# Real data
Nbins = 10
RS1 = group_RS_timings[:, :, 3]
group_res2 = np.zeros((Ns, 50, 5))*np.nan
for i in range(Ns):
    negpeaks = group_negpeaks[i, :]
    Nseg = int(np.ceil(np.sum(~np.isnan((RS1[i, :]))) / 40))
    
    for j in range(Nseg):
        irr = np.nanmean(1/group_ihr[i, j*40:(j+1)*40])
        rs = RS1[i, j*40:(j+1)*40]
        rs = np.abs(rs[~np.isnan(rs)])
        hist, be = np.histogram(rs, range=(0, irr), bins=Nbins)
        hist = hist / len(rs)
        entr = entropy(hist, Nbins)
        group_res2[i, j, 0] = entr
        p = stats.percentileofscore(perms, entr)
        group_res2[i, j, 1] = p
        group_res2[i, j, 2] = np.nanmean(negpeaks[j*40:(j+1)*40])
        group_res2[i, j, 3] = np.mean(rs)
        group_res2[i, j, 4] = fxconc[int(np.nanmean(negpeaks[j*40:(j+1)*40]))]
        
## Plot data
plt.rc('font', size=20)
fig, axs = plt.subplots(2, 2, figsize=(12, 8))
axx, axx2, axx3, axx4 = axs.flatten()

axx.annotate('A', xy=(-3.5, 50), fontsize=40, annotation_clip=False)
axx.annotate('B', xy=(2.75, 50), fontsize=40, annotation_clip=False)
axx.annotate('C', xy=(-3.5, -110), fontsize=40, annotation_clip=False)
axx.annotate('D', xy=(2.75, -110), fontsize=40, annotation_clip=False)

# A: EEG
Nsw = np.sum(np.sum(~np.isnan(group_negpeaks), 1))
subs = np.arange(16)   
tt = np.linspace(-2, 2, 399)
tt2 = np.linspace(-2, 2, 1999)
mn1 = np.nanmean(group_eeg_wf[subs, :]/1, 0)
er1 = np.nanstd(group_eeg_wf[subs, :], 0) / np.sqrt(Ns)

mn2 = np.nanmean(group_ecg_wf[subs, :], 0)
er2 = np.nanstd(group_ecg_wf[subs, :], 0) / np.sqrt(Ns)

mn3 = np.nanmean(group_lf_ecg_wf[subs, :], 0)
er3 = np.nanstd(group_lf_ecg_wf[subs, :], 0) / np.sqrt(Ns)

for i in range(Ns):
    axx.plot(tt, group_eeg_wf[i, :], c='k', alpha=0.25, zorder=1)
    
axx.plot(tt, mn1, c='b', label='EEG', lw=3, zorder=800)
axx.set_yticks([-50, 0, 50])
axx.set_xticks([-2, -1, 0, 1, 2])
axx.set_xlabel('Time from slow wave start [s]')
axx.set_ylabel('EEG [μV]', labelpad=-5)
axx.spines['right'].set_visible(False)
axx.spines['top'].set_visible(False)

# B: ECG
print('ECG peak to SW start delay %.5f s' %(tt2[np.argmax(mn2)]))
axx2.plot(tt, mn1/2, c='k', label='Slow \nwave', lw=2, alpha=1, zorder=3)
axx2.plot(tt2, mn2, c='r', label='ECG', lw=1, alpha=0.75, zorder=3)
axx2.fill_between(tt2, mn2+er2, mn2-er2, alpha=0.5, color='r', zorder=3)
axx2.plot(tt2, mn3, c='purple', label='LF-ECG', lw=2, zorder=5)
axx2.fill_between(tt2, mn3+er3, mn3-er3, alpha=0.5, color='purple', zorder=5)
axx2.set_yticks([-10, 0, 10])
axx2.set_xticks([-2, -1, 0, 1, 2])
axx2.set_ylabel('ECG [μV]', labelpad=-5)
axx2.spines['right'].set_visible(False)
axx2.spines['top'].set_visible(False)
axx2.set_xlabel('Time from slow wave start [s]')
leg=axx2.legend(ncol=3, fontsize=12, bbox_to_anchor=(1, 1.15), loc=1)
for line in leg.get_lines():
    line.set_linewidth(4)

# C: Entropy of RS-1, Galletly style
entropies = np.nanmean(group_res2[:, :, 0], 1)
Nbins = 90 
axx3.hist(perms, bins=Nbins, range=(0.75, 1), color='skyblue', ec='k', lw=0.25, 
          density=True, label='Surrogate\nDistribution')
axx3.axvline(entropies[5], c='k', lw=1, label='Individual\nsubjects')

for s in range(Ns):
    axx3.axvline(entropies[s], c='gray', lw=1)
axx3.axvline(np.nanmean(group_res2[:, :, 0]), c='r', lw=2)
axx3.spines['right'].set_visible(False)
axx3.spines['top'].set_visible(False)
axx3.set_ylabel('Histogram')
axx3.set_xlabel('RS$_{-1}$ Proportional Entropy')
axx3.set_xticks([0.8, 0.9, 1])
axx3.set_xlim(0.8, 1.01)
axx3.legend(ncol=2, fontsize=12, bbox_to_anchor=(0.95, 1.3), loc=1)

# D: HR vs SWF
hrs = np.nanmean(group_ihr, 1) * 60
swfs = np.nanmean(group_sw_freqs, 1)
Y = swfs
X = hrs
X = sm.add_constant(X)
model = sm.OLS(Y, X)
res = model.fit()
ypred = res.predict(X)
print('SWF~HR params:', res.params, res.bse)

axx4.scatter(hrs, swfs, c='r')
axx4.plot(hrs, ypred, c='k')
axx4.set_xticks([60, 70, 80, 90, 100])
axx4.set_ylabel('Slow-wave\nFrequency [Hz]')
axx4.set_xlabel('Heart Rate [bpm]')
axx4.spines['right'].set_visible(False)
axx4.spines['top'].set_visible(False)
    
plt.subplots_adjust(hspace=0.66, wspace=0.5)

if save:
    fname = './Figures/fig_5.jpg'
    fig.savefig(fname, format='jpg', bbox_inches = 'tight', dpi=600)
    
plt.show()


#%% APPENDIX 2 ---------------------------------------------------------------
## Prepare data
hrs = group_swa_hr[:, 1, :]
lobrs = np.squeeze(io.loadmat('./Data/Propofol_LOBRs.mat')['lors'])
hrs_comp = np.zeros((16, 3))
Nsec = 60
for i in range(16):
    lob = int(lobrs[i])
    peak = np.argmax(fxconc)
    hr_base = np.nanmean(hrs[i, :300])
    hr_lobr = np.nanmean(hrs[i, lob-Nsec:lob+Nsec])
    hr_peak = np.nanmean(hrs[i, peak-Nsec:peak+Nsec])
    hrs_comp[i, 0] = hr_base
    hrs_comp[i, 1] = hr_lobr
    hrs_comp[i, 2] = hr_peak

# P-values
_, p1 = stats.ttest_rel(hrs_comp[:, 0], hrs_comp[:, 1])
_, p2 = stats.ttest_rel(hrs_comp[:, 0], hrs_comp[:, 2])
_, p3 = stats.ttest_rel(hrs_comp[:, 1], hrs_comp[:, 2])
ps = np.array([p1, p2, p3])
ps_corr = ps / 3

df = pd.DataFrame(hrs_comp)

## Plot data
plt.rc('font', size=24)
fig, ax = plt.subplots(figsize=(8, 6))
sns.violinplot(data=df, ax=ax, inner='points', zorder=10)
ax.set_xticklabels(['Baseline', 'LOBR', 'Peak'])
ax.plot([0, 1], [109, 109], c='k', lw=4)
ax.plot([1, 2], [113, 113], c='k', lw=4)
ax.plot([0, 2], [104, 104], c='k', lw=4)
ax.annotate('**', xy=(0.43, 109), fontsize=20, annotation_clip=False)
ax.annotate('***', xy=(0.9, 103.5), fontsize=20, annotation_clip=False)
ax.annotate('***', xy=(1.42, 113), fontsize=20, annotation_clip=False)

for i in range(16):
    ax.plot(hrs_comp[i, :], c='r', zorder=1)
    ax.scatter([0, 1, 2], hrs_comp[i, :], c='k', zorder=10)
ax.set_ylabel('Heart Rate [bpm]')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
if save:
    fname = './Figures/supp_fig_hr_lobr.jpg'
    fig.savefig(fname, format='jpg', bbox_inches = 'tight', dpi=600)
    

#%% SUPPLEMENTARY FIGURE 5 ---------------------------------------------------
## Prepare data
# Generate surrogate uniform ACF distribution
Nperm = 1000
acf_uni_all = np.zeros((Nperm, Nbins-1))
for p in range(Nperm):
    np.random.seed(p)
    uni = np.random.uniform(-lim, lim, 10000)
    hist_uni, be = np.histogram(uni, range=(-lim, lim), bins=Nbins, density=True)
    acf_uni = sm.tsa.stattools.acf(hist_uni, nlags=Nbins)[1:] #Ignore trivial lag 0 correlation
    acf_uni_all[p, :] = acf_uni
th = np.percentile(np.abs(acf_uni_all.flatten()), 99.9)

subs = np.arange(16)
th999 = np.percentile(np.abs(acf_uni_all.flatten()), 99.9)
th99 = np.percentile(np.abs(acf_uni_all.flatten()), 99)
th95 = np.percentile(np.abs(acf_uni_all.flatten()), 95)

exp_decay = lambda x, A, c, f, x0: A * np.exp(c*x) * np.sin(2*np.pi*f*(x-x0))
sig_acf = np.zeros(Ns)
max_acf = np.zeros(Ns)
pvals_ks = np.zeros(Ns)
group_hist = np.zeros((Ns, Nbins))*np.nan
group_hist_norm = np.zeros((Ns, Nbins))*np.nan

## Plot data
plt.rc('font', size=16)
sig_acf = np.zeros(Ns)
results = np.zeros((Ns, 4))*np.nan
fig, axs = plt.subplots(4, 4, figsize=(12, 8))
for i in range(Ns):
    rs = group_RS_timings[i, :, :].flatten()
    rs = rs[~np.isnan(rs)]
    rs = rs[np.abs(rs)<lim]    
    hist, be = np.histogram(rs, range=(-lim, lim), bins=Nbins, density=True)
    
    ihr = np.nanmean(group_ihr, 1)[i]
    hist_norm, be_norm = np.histogram(rs*ihr, range=(-lim, lim), bins=Nbins, density=True)
    
    acf_norm = sm.tsa.stattools.acf(hist, nlags=Nbins)[1:]
    lags = np.linspace(0, 6, len(acf_norm))
    max_acf[i] = np.max(np.abs(acf_norm))
    
    # Fit decaying sinusoid to ACF
    guess = [0.4, -0.3, ihr, 0]
    params, cov = curve_fit(exp_decay, lags, acf_norm, p0=guess, 
                            bounds=([-1, -1, ihr*0.8, -1], [1, 0, ihr*1.2, 1]))
    ypred = exp_decay(lags, *params)
    r2 = r2_score(acf_norm, ypred)

    group_hist[i, :] = hist
    group_hist_norm[i, :] = hist_norm
    
    ax = axs.flatten()[i]
    ax.plot(lags, acf_norm)
    ax.plot(lags, ypred)
    ax.set_title('$S%.0f, R^2=%.2f$'%(i+1, r2), fontsize=14)
    ax.set_xlabel('Lag [s]', labelpad=-2)
    ax.set_xticks([0, 2, 4, 6])
    ax.set_ylabel('Acorr', labelpad=-5)
    ax.set_ylim(-0.55, 0.55)
    ax.axhline(th95, c='hotpink', ls=':', label='P=0.05')
    ax.axhline(-1*th95, c='hotpink', ls=':')
    ax.axhline(th99, c='orangered', ls='--', label='P=0.01')
    ax.axhline(-1*th99, c='orangered', ls='--')
    ax.axhline(th999, c='firebrick', label='P=0.001')
    ax.axhline(-1*th999, c='crimson')
    
    stat, pval = stats.bartlett(ypred-acf_norm, acf_norm-np.mean(acf_norm))
    results[i, 2] = pval
    if pval < 0.05:
        results[i, 0] = lags[np.argmin(acf_norm[lags<1])]
        results[i, 1] = params[2]
        ax.axvline(lags[np.argmin(acf_norm[lags<1])], color='k')
        results[i, 2] = pval
        results[i, 3] = r2
        ax.set_title('$S%.0f, R^2=%.2f$'%(i+1, r2), color='red', fontsize=14)
    
plt.subplots_adjust(hspace=1.2, wspace=0.75)

if save:
    fname = './Figures/supp_fig_acorr.jpg'
    fig.savefig(fname, format='jpg', bbox_inches = 'tight', dpi=600)

plt.show()

#%% SUPPLEMENTARY FIGURE 4 ---------------------------------------------------
plt.rc('font', size=20)
fig = plt.figure(figsize=(8, 5)) 
axx = fig.add_axes([0, 0.5, 0.45, 0.45])
axx2 = fig.add_axes([0.6, 0.5, 0.45, 0.45])
axx3 = fig.add_axes([1.2, 0.5, 0.45, 0.45])
ax2 = fig.add_axes([0.3, -0.25, 0.45, 0.45])
ax3 = fig.add_axes([0.9, -0.25, 0.45, 0.45])

axx.annotate('A', xy=(-3.5, 55), fontsize=40, annotation_clip=False)
axx.annotate('B', xy=(2.4, 55), fontsize=40, annotation_clip=False)
axx.annotate('C', xy=(7.95, 55), fontsize=40, annotation_clip=False)
axx.annotate('D', xy=(-1.2, -120), fontsize=40, annotation_clip=False)

# A: EEG
Nsw = np.sum(np.sum(~np.isnan(group_negpeaks), 1))
subs = np.arange(16)   
tt = np.linspace(-2, 2, 399)
tt2 = np.linspace(-2, 2, 1999)
mn1 = np.nanmean(group_eeg_wf[subs, :]/1, 0)
er1 = np.nanstd(group_eeg_wf[subs, :], 0) / np.sqrt(Ns)

mn2 = np.nanmean(group_ecg_wf[subs, :], 0)
er2 = np.nanstd(group_ecg_wf[subs, :], 0) / np.sqrt(Ns)

mn3 = np.nanmean(group_lf_ecg_wf[subs, :], 0)
er3 = np.nanstd(group_lf_ecg_wf[subs, :], 0) / np.sqrt(Ns)

for i in range(Ns):
    axx.plot(tt, group_eeg_wf[i, :], c='k', alpha=0.25, zorder=1)
    
axx.plot(tt, mn1, c='b', label='EEG', lw=3, zorder=800)
axx.set_yticks([-50, 0, 50])
axx.set_xticks([-2, -1, 0, 1, 2])
axx.set_xlabel('Time from slow wave start [s]')
axx.set_ylabel('EEG [μV]', labelpad=-5)
axx.spines['right'].set_visible(False)
axx.spines['top'].set_visible(False)

# B: ECG
print('ECG peak to SW start delay %.5f s' %(tt2[np.argmax(mn2)]))
axx2.plot(tt, mn1/2, c='k', label='Slow \nwave', lw=2, alpha=1, zorder=3)
axx2.plot(tt2, mn2, c='r', label='ECG', lw=1, alpha=0.75, zorder=3)
axx2.fill_between(tt2, mn2+er2, mn2-er2, alpha=0.5, color='r', zorder=3)
axx2.plot(tt2, mn3, c='purple', label='LF-ECG', lw=2, zorder=5)
axx2.fill_between(tt2, mn3+er3, mn3-er3, alpha=0.5, color='purple', zorder=5)
axx2.set_yticks([-10, 0, 10])
axx2.set_xticks([-2, -1, 0, 1, 2])
axx2.set_ylabel('ECG [μV]', labelpad=-5)
axx2.spines['right'].set_visible(False)
axx2.spines['top'].set_visible(False)
axx2.set_xlabel('Time from slow wave start [s]')
leg=axx2.legend(ncol=3, fontsize=11, bbox_to_anchor=(1, 1.1))
for line in leg.get_lines():
    line.set_linewidth(4)

# C: R-wave incidence histogram
x = 0.5*(be[1:] + be[:-1])
m = np.nanmean(group_hist_norm[:, :], 0)
acfm = sm.tsa.stattools.acf(m, nlags=Nbins)
acfuni = sm.tsa.stattools.acf(hist_uni, nlags=Nbins)

axx3.bar(x, m, width=6/Nbins/1.5, color='skyblue', ec='k', lw=0.1)
axx3.set_xlabel('Normalised time [s]')
axx3.set_ylabel('R-wave incidence')
axx3.set_yticks([0, 0.1, 0.2])
axx3.set_xlim(-3, 3)
axx3.spines['right'].set_visible(False)
axx3.spines['top'].set_visible(False)

# D: Autocorrelations
lags = signal.correlation_lags(len(m), len(m), mode='full')/(Nbins/6)
lags = lags[90:]
markerline, stemlines, baseline = ax2.stem(lags, acfm[1:], linefmt='k', markerfmt='k.')

params, cov = curve_fit(exp_decay, lags, acfm[1:], p0=guess)
ypred = exp_decay(lags, *params)
ax2.plot(lags, ypred, c='gray')
plt.setp(stemlines, 'linewidth', 1)

markerline, stemlines, baseline = ax3.stem(lags, acfuni[1:], linefmt='k', markerfmt='k.')
plt.setp(stemlines, 'linewidth', 1)

ax2.axhline(th95, c='hotpink', ls=':', label='P=0.05')
ax2.axhline(-1*th95, c='hotpink', ls=':')
ax2.axhline(th99, c='orangered', ls='--', label='P=0.01')
ax2.axhline(-1*th99, c='orangered', ls='--')
ax2.axhline(th999, c='firebrick', label='P=0.001')
ax2.axhline(-1*th999, c='crimson')

ax3.axhline(th95, c='hotpink', ls=':', label='P=0.05')
ax3.axhline(-1*th95, c='hotpink', ls=':')
ax3.axhline(th99, c='orangered', ls='--', label='P=0.01')
ax3.axhline(-1*th99, c='orangered', ls='--')
ax3.axhline(th999, c='firebrick', label='P=0.001')
ax3.axhline(-1*th999, c='crimson')
leg = ax3.legend(ncol=3, bbox_to_anchor=(0.82, -0.26))
for line in leg.get_lines():
    line.set_linewidth(4)
    
ax.set_xlabel('Normalised time \nfrom slow wave start [s]')
ax.set_ylabel('R-wave incidence')
ax2.set_ylim(-0.5, 0.5)
ax2.set_xlabel('Lag [s]')
ax2.set_xticks([0, 2, 4, 6])
ax2.set_title('Data')
ax2.set_yticks([-0.5, 0, 0.5])
ax2.set_ylabel('Autocorrelation', labelpad=0)
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)

ax3.set_ylim(-0.5, 0.5)
ax3.set_xlabel('Lag [s]')
ax3.set_xticks([0, 2, 4, 6])
ax3.set_title('Random surrogate')
ax3.set_ylabel('Autocorrelation', labelpad=0)
ax3.set_yticks([-0.5, 0, 0.5])
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)

if save:
    fname = './Figures/supp_fig_ecg_sw_acorr.jpg'
    fig.savefig(fname, format='jpg', bbox_inches = 'tight', dpi=600)

plt.show()


#%% SUPPLEMENTARY FIGURE 6 ---------------------------------------------------
the = np.percentile(perms, 1)

plt.rc('font', size=16)
fig, axs = plt.subplots(4, 4, figsize=(12, 8))
for i in range(Ns):
    ax = axs.flatten()[i]
    Nseg = np.sum(group_res2[i, :, 0]>0) 
    ax.plot(group_res2[i, :Nseg, 2]/60, group_res2[i, :Nseg, 0], c='k')
    ax.axhline(the, c='r')
    ax.set_ylim(0.75, 1.01)
    ax.set_xticks([0, 30, 60, 90, 120])
    ax.set_yticks([0.8, 0.9, 1])
    ax2 = ax.twinx()
    ax2.plot(np.linspace(0, 116, 116*60), fxconc, c='grey', lw=2)
    ax2.set_yticks([0, 2, 4])
    ax.set_title('S%.0f'%(i+1))
plt.subplots_adjust(hspace=0.66, wspace=0.5)

fig.text(0.5, 0.05, 'Time [min]', ha='center')
fig.text(0.06, 0.5, 'Proportional Entropy', va='center', rotation='vertical')
fig.text(0.94, 0.5, 'Propofol [µg/ml]', va='center', rotation='vertical')

print('SH_P = %.3f +/- %.3f' % (np.nanmean(group_res2[:, :, 0]), np.nanstd(group_res2[:, :, 0])))
if save:
    fname = './Figures/supp_fig_ecg_sw_entropies.jpg'
    fig.savefig(fname, format='jpg', bbox_inches = 'tight', dpi=600)
    

#%% Display number of slow waves detected
Nsw = np.sum(~np.isnan(group_negpeaks), 1)
print('Nsw:',Nsw)
print('Nsw = %.2f +/- %.2f' %(np.mean(Nsw), np.std(Nsw)/np.sqrt(16)))

t, p = stats.ttest_ind(Nsw[results[:, 2]>0.05], Nsw[results[:, 2]<0.05])
print(Nsw[results[:, 2]>0.05])
print(Nsw[results[:, 2]<0.05])